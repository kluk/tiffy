# TIFFY - a GUI for ffmpeg

This Readme is only maintained for Windows.

## Dependencies
  - ffmpeg from https://www.gyan.dev/ffmpeg/builds/
    - Add to PATH
  - [Inno Setup](https://jrsoftware.org/isinfo.php)
    - Add to PATH
  - [poetry](https://python-poetry.org/)
  - [7zip](https://www.7-zip.org/)
    - Add to PATH

## Installer

This project uses pyinstaller and Inno Setup to create an installer
for Windows systems.

Run `poe release` to create an installer.
See [bin/README](bin/README.md) for details.
