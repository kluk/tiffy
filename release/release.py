import logging
import os
import shutil
import subprocess
import sys
from pathlib import Path

import requests
import toml
from tqdm import tqdm

root_dir = Path(__file__).parent.parent.absolute()
bin_dir = root_dir / "bin"
ffmpeg = bin_dir / "ffmpeg.exe"
ffplay = bin_dir / "ffplay.exe"
ffprobe = bin_dir / "ffprobe.exe"
ffmpeg_archive = bin_dir / "ffmpeg.7z"
ffmpeg_extracted = bin_dir / "tmp"

os.chdir(root_dir)

with open(root_dir / "pyproject.toml", "r") as tiffy:
    values = toml.load(tiffy)
    appname = values["tool"]["poetry"]["name"]
    version = values["tool"]["poetry"]["version"]

os.environ.setdefault("TIFFY_APPNAME", appname)
os.environ.setdefault("TIFFY_VERSION", version)

if not ffmpeg.is_file() or not ffplay.is_file() or not ffprobe.is_file():
    ffmpeg.unlink(missing_ok=True)
    ffplay.unlink(missing_ok=True)
    ffprobe.unlink(missing_ok=True)
    # This url might not work in the future
    url = "https://www.gyan.dev/ffmpeg/builds/ffmpeg-release-full.7z"
    response = requests.get(url, stream=True)

    with open(ffmpeg_archive, "wb") as handle:
        for data in tqdm(response.iter_content(chunk_size=1000)):
            handle.write(data)

    shutil.rmtree(ffmpeg_extracted, ignore_errors=True)
    ffmpeg_extracted.unlink(missing_ok=True)
    os.mkdir(ffmpeg_extracted)
    result = subprocess.run(["7z", "x", str(ffmpeg_archive), f"-o{ffmpeg_extracted}"])
    if result.returncode != 0:
        logging.error("Failed to extract %s", ffmpeg_archive)
        sys.exit(result.returncode)
    ffmpeg_bin = bin_dir / "tmp" / os.listdir(ffmpeg_extracted)[0] / "bin"
    shutil.copy(ffmpeg_bin / "ffmpeg.exe", ffmpeg)
    shutil.copy(ffmpeg_bin / "ffplay.exe", ffplay)
    shutil.copy(ffmpeg_bin / "ffprobe.exe", ffprobe)

result = subprocess.run(["pyinstaller", "__main__.spec", "--noconfirm"])
if result.returncode != 0:
    logging.error("Failed to run pyinstaller")
    sys.exit(result.returncode)

result = subprocess.run(["iscc", "setup.iss"])
if result.returncode != 0:
    logging.error("Failed to run Inno Setup")
    sys.exit(result.returncode)
