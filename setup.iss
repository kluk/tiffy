#define APPNAME GetEnv('TIFFY_APPNAME')
#define VERSION GetEnv('TIFFY_VERSION')

[Setup]
AppName={#APPNAME}
AppVersion={#VERSION}
AppId={{67070B91-D6B6-42ED-81A7-78059AEB0A72}
OutputBaseFilename={#APPNAME}_setup_{#VERSION}
WizardStyle=modern
DefaultDirName={autopf}\{#APPNAME}
DefaultGroupName={#APPNAME}
UninstallDisplayIcon={app}\{#APPNAME}.exe
Compression=lzma2
SolidCompression=yes
OutputDir=.\

[Files]
Source: "dist\tiffy\*"; DestDir: "{app}\"; Flags: ignoreversion recursesubdirs
Source: ".\bin\ffmpeg.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: ".\bin\ffplay.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: ".\bin\ffprobe.exe"; DestDir: "{app}"; Flags: ignoreversion

[Icons]
Name: "{group}\{#APPNAME}"; Filename: "{app}\{#APPNAME}.exe"
Name: "{commondesktop}\{#APPNAME}"; Filename: "{app}\{#APPNAME}.exe";

[Run]
Filename: {app}\{#APPNAME}.exe; Description: {cm:LaunchProgram,{#APPNAME}}; Flags: nowait postinstall skipifsilent

[UninstallDelete]
Type: filesandordirs; Name: "{app}\assets"
Type: filesandordirs; Name: "{app}\Include"
Type: filesandordirs; Name: "{app}\lib2to3"
Type: filesandordirs; Name: "{app}\psutil"
Type: filesandordirs; Name: "{app}\PySide2"
Type: filesandordirs; Name: "{app}\shiboken2"
Type: filesandordirs; Name: "{app}\tcl"
Type: filesandordirs; Name: "{app}\tk"
Type: filesandordirs; Name: "{app}\win32com"
