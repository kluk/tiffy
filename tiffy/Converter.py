import logging
import re
import subprocess
import tempfile
import threading
from pathlib import Path
from re import Match
from typing import IO, AnyStr, Optional

import ffmpeg
from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtCore import Slot
from PySide2.QtWidgets import (
    QFileDialog,
    QLineEdit,
    QProgressBar,
    QPushButton,
    QVBoxLayout,
    QWidget,
)
from tiffy import TabStream, probe
from tiffy.DetailsViewer import DetailsViewer
from tiffy.probe import (
    AudioStream,
    SubtitleStream,
    VideoStream,
    get_stream_time_in_seconds,
)
from tiffy.VideoStream import VideoStreamSetting


class DataInfo(QWidget):
    def __init__(self, path, video_streams: list):
        QWidget.__init__(self)

        self.layout = QVBoxLayout()

        self.progressbar = QProgressBar()
        self.layout.addWidget(self.progressbar)
        self.label = QtWidgets.QLabel()
        if len(video_streams) > 0:
            width = int(video_streams[0]["width"])
            seconds = get_stream_time_in_seconds(video_streams[0])
            image_path = tempfile.gettempdir() + "\\tmp.png"

            (
                ffmpeg.input(path, ss=seconds / 2)
                .filter("scale", width, -1)
                .output(image_path, vframes=1)
                .run(overwrite_output=True, quiet=True)
            )
            pixmap = QtGui.QPixmap(image_path)
            pixmap = pixmap.scaled(500, 500, QtCore.Qt.KeepAspectRatio)
            self.label.setPixmap(pixmap)

        self.label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.layout.addWidget(self.label)

        self.details = DetailsViewer(path)
        self.layout.addWidget(self.details)

        self.setWindowTitle(path)
        self.setLayout(self.layout)


class FfmpegProcess:
    def __init__(self, cmd: list[str]):
        self.process = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            universal_newlines=True,
        )

    def __enter__(self) -> IO[AnyStr]:
        return self.process.stdout

    def __exit__(self, exc_type, exc_value, traceback) -> bool:
        logging.warning("Encoding stopped")
        self.process.kill()
        return False


class Encoder(threading.Thread):
    def __init__(self, cmd: list[str], converter):
        super(Encoder, self).__init__()
        self._stop_event = threading.Event()
        self.cmd = cmd
        self.converter = converter
        self.result: bool = False

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

    def run(self):
        converter = self.converter
        try:
            duration = float(ffmpeg.probe(converter.path)["format"]["duration"])
            num_frames = int(
                probe.get_avg_fps(converter.tabstream.video_streams.streams[0].stream)
                * duration
            )
            width = int(converter.tabstream.video_streams.streams[0].stream["width"])

            with FfmpegProcess(cmd=self.cmd) as stdout:
                for line in stdout:
                    if self.stopped():
                        break

                    match: Optional[Match] = re.search(
                        pattern=r"^frame=\s*(\d*)\s*fps=\s*([0-9]*[.,]?[0-9]*)\s*q=.*$",
                        string=line,
                    )
                    if match is None or len(match.groups()) != 2:
                        continue

                    frame = int(match.group(1))
                    fps = float(match.group(2))

                    progress = int(frame / num_frames * 100.0)
                    converter.datainfo.progressbar.setValue(progress)
                    converter.datainfo.progressbar.setFormat(f"{progress}% {fps}fps")
                    image_path = Path(tempfile.gettempdir()) / "tmp.png"

                    (
                        ffmpeg.input(converter.path, ss=frame / num_frames * duration)
                        .filter("scale", width, -1)
                        .output(filename=image_path, vframes=1)
                        .run(overwrite_output=True, quiet=True)
                    )
                    pixmap = QtGui.QPixmap(str(image_path))
                    pixmap = pixmap.scaled(500, 500, QtCore.Qt.KeepAspectRatio)
                    converter.datainfo.label.setPixmap(pixmap)
                    converter.update()
        except BaseException:
            return
        self.result = True

    def join(self, *args) -> bool:
        threading.Thread.join(self)
        return self.result


class Converter(QWidget):
    def __init__(
        self,
        path: Path,
        video_streams: list[VideoStream],
        audio_streams: list[AudioStream],
        subtitle_streams: list[SubtitleStream],
    ):
        QWidget.__init__(self)
        self.layout = QVBoxLayout()

        self.path = path
        self.videostreamsetting = VideoStreamSetting(self)
        self.layout.addWidget(self.videostreamsetting)
        self.tabstream = TabStream.TabStream(
            self, video_streams, audio_streams, subtitle_streams
        )
        self.datainfo = DataInfo(path, video_streams)
        self.layout.addWidget(self.tabstream)
        self.layout.addWidget(self.datainfo)

        self.commandline = QLineEdit()

        self.gobutton = QPushButton("GO")
        self.layout.addWidget(self.commandline)
        self.layout.addWidget(self.gobutton)

        self.stopbutton = QPushButton("STOP")
        self.layout.addWidget(self.stopbutton)

        self.setLayout(self.layout)

        # Connecting the signal
        self.gobutton.clicked.connect(self.magic)
        self.stopbutton.clicked.connect(self.stop_processes)
        self.update_commandline()
        self.thread: Optional[Encoder] = None

    def closeEvent(self, event):
        # self.killProcesses()
        event.accept()

    def get_ffmpeg(
        self, filename="test-out.mkv"
    ) -> Optional[ffmpeg.nodes.OutputStream]:
        input_file_url = ffmpeg.input(self.path)
        bit_rate = int(self.videostreamsetting.bitrate.bitrate_edit.text()) * 1000

        out_streams = []
        for stream in (
            self.tabstream.video_streams.streams
            + self.tabstream.audio_streams.streams
            + self.tabstream.subtitle_streams.streams
        ):
            if stream.checkbox.isChecked():
                out_streams.append(input_file_url[str(stream.stream["index"])])

        if not out_streams:
            return None

        codec = self.videostreamsetting.codec.currentText()

        if self.videostreamsetting.copycheckbox.isChecked():
            outfile = ffmpeg.output(*out_streams, filename=filename)
            outfile = outfile.global_args("-c:a", "copy")
            outfile = outfile.global_args("-c:v", "copy")
            outfile = outfile.global_args("-c:s", "copy")
        else:
            outfile = ffmpeg.output(
                *out_streams,
                filename=filename,
                video_bitrate=str(bit_rate),
                vcodec=codec,
            )
            outfile = outfile.global_args("-c:a", "copy")
            outfile = outfile.global_args("-c:s", "copy")

        return outfile

    @Slot()
    def magic(self):
        filename = QFileDialog.getSaveFileName(
            self, caption="Select output file", dir="../", filter="Matroska (*.mkv)"
        )[0]
        ffm = self.get_ffmpeg(filename=filename)
        print(type(ffm))
        if ffm is not None:
            cmd = ffmpeg.compile(ffm)
            cmd.append("-y")
            cmd.append("-hwaccel")
            cmd.append("auto")

            if (
                (self.videostreamsetting.codec.currentText() in "libx265")
                and (self.videostreamsetting.hdrfix is not None)
                and (self.videostreamsetting.hdrfix.chkbox.isChecked())
            ):
                libx265opts = str(self.videostreamsetting.hdrfix.line.text())
                libx265opts = libx265opts.split(" ")
                for opt in libx265opts:
                    cmd.append(opt)

            if self.thread is not None:
                self.thread.stop()
            self.thread = Encoder(cmd=cmd, converter=self)
            self.gobutton.setDisabled(True)
            self.thread.start()

        self.gobutton.setDisabled(False)

    @Slot()
    def update_commandline(self):
        if not hasattr(self, "commandline"):
            return
        ffm = self.get_ffmpeg()
        if ffm is not None:
            line = ffmpeg.compile(self.get_ffmpeg())
            self.commandline.setText(str(line))
        else:
            self.commandline.setText("")

    @Slot()
    def stop_processes(self):
        self.gobutton.setDisabled(False)
        if self.thread is None:
            return
        self.thread.stop()
        finished: bool = self.thread.join()
        self.datainfo.progressbar.setValue(0)
        self.datainfo.progressbar.setFormat("0%")
        if finished:
            self.datainfo.progressbar.setValue(100)
            self.datainfo.progressbar.setFormat("100%")
        self.update()
        self.thread = None
