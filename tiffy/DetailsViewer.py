import ffmpeg
from PySide2.QtCore import Slot
from PySide2.QtWidgets import (
    QPushButton,
    QTextEdit,
    QTreeWidget,
    QTreeWidgetItem,
    QVBoxLayout,
    QWidget,
)


class DetailsViewer(QWidget):
    def __init__(self, path):
        QWidget.__init__(self)
        self.layout = QVBoxLayout()

        self.showMe = QPushButton("Show Metadata")
        self.layout.addWidget(self.showMe)

        self.setLayout(self.layout)

        self.data = ffmpeg.probe(path)
        self.browser = QTextEdit()
        self.currentText = ""
        self.tree = QTreeWidget()

        from tiffy.probe import get_hdr_meta_data

        hdr_meta_data = get_hdr_meta_data(path)

        if hdr_meta_data is not None:
            for key in hdr_meta_data:
                widget = QTreeWidgetItem([str(key)])
                widget.addChild(QTreeWidgetItem([str(hdr_meta_data[key])]))
                self.tree.addTopLevelItem(widget)

        def append(potentialList, depth, viewer):
            if type(potentialList) is dict:
                for key in potentialList:
                    for i in range(0, depth):
                        viewer.currentText = viewer.currentText + "\t"
                    viewer.currentText = viewer.currentText + str(key) + "\n"
                    append(potentialList[key], depth + 1, viewer)
            elif type(potentialList) is list:
                for element in potentialList:
                    append(element, depth, viewer)
            else:
                for i in range(0, depth):
                    viewer.currentText = viewer.currentText + "\t"
                viewer.currentText = viewer.currentText + str(potentialList) + "\n"

            return

        def buildTree(potentialList, depth, viewer, currentItem):
            if type(potentialList) is dict:
                for key in potentialList:
                    item = None
                    if depth == 0:
                        item = QTreeWidgetItem([str(key)])
                        viewer.tree.addTopLevelItem(item)
                    else:
                        item = QTreeWidgetItem([str(key)])
                        currentItem.addChild(item)
                    buildTree(potentialList[key], depth + 1, viewer, item)
            elif type(potentialList) is list:
                i = 0
                for element in potentialList:
                    item = None
                    if depth == 0:
                        item = QTreeWidgetItem([str(i)])
                        viewer.tree.addTopLevelItem(item)
                    else:
                        item = QTreeWidgetItem([str(i)])
                        currentItem.addChild(item)
                    buildTree(element, depth, viewer, item)
                    i = i + 1
            else:
                item = QTreeWidgetItem([str(potentialList)])
                if depth == 0:
                    viewer.tree.addTopLevelItem(item)
                else:
                    currentItem.addChild(item)

            return

        append(self.data, 0, self)
        buildTree(self.data, 0, self, None)
        self.browser.setText(self.currentText)

        self.browser.hide()
        self.tree.hide()

        self.showMe.clicked.connect(self.toggleMetadata)

    @Slot()
    def toggleMetadata(self):
        # if self.browser.isHidden():
        #    self.browser.show()
        # else:
        #    self.browser.hide()

        if self.tree.isHidden():
            self.tree.show()
        else:
            self.tree.hide()
