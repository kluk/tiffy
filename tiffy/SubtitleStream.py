from PySide2 import QtCore
from PySide2.QtCore import Slot
from PySide2.QtWidgets import QCheckBox, QHBoxLayout, QLabel, QVBoxLayout, QWidget
from tiffy import probe


class SubtitleStreamWidget(QWidget):
    def __init__(self, converter, stream):
        QWidget.__init__(self)

        self.stream = stream
        self.layout = QHBoxLayout()
        self.checkbox = QCheckBox()
        self.layout.addWidget(self.checkbox)
        repr = probe.subtitle_stream_representation(stream)
        if len(repr) > 72:
            repr = repr[0:72] + "..."
        element = QLabel(repr)
        self.layout.addWidget(element)
        self.setLayout(self.layout)
        self.converter = converter
        self.checkbox.stateChanged.connect(self.update)
        self.checkbox.setCheckState(QtCore.Qt.Checked)

    @Slot()
    def update(self):
        self.converter.update_commandline()


class SubtitleStreamsWidget(QWidget):
    def __init__(self, converter, streams):
        QWidget.__init__(self)
        self.layout = QVBoxLayout()
        self.streams = []
        for stream in streams:
            widget = SubtitleStreamWidget(converter, stream)
            self.streams.append(widget)
            self.layout.addWidget(widget)
        self.setLayout(self.layout)
