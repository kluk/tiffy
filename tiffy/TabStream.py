from PySide2.QtWidgets import QTabWidget
from tiffy.AudioStream import AudioStreamsWidget
from tiffy.probe import AudioStream, SubtitleStream, VideoStream
from tiffy.SubtitleStream import SubtitleStreamsWidget
from tiffy.VideoStream import VideoStreamsWidget


class TabStream(QTabWidget):
    def __init__(
        self,
        converter,
        video_streams: list[VideoStream],
        audio_streams: list[AudioStream],
        subtitle_streams: list[SubtitleStream],
    ):
        QTabWidget.__init__(self)

        self.video_streams = VideoStreamsWidget(converter, video_streams)
        self.audio_streams = AudioStreamsWidget(converter, audio_streams)
        self.subtitle_streams = SubtitleStreamsWidget(converter, subtitle_streams)

        self.addTab(self.video_streams, "Video")
        self.addTab(self.audio_streams, "Audio")
        self.addTab(self.subtitle_streams, "Subtitles")
        self.setWindowTitle("Available Streams")
