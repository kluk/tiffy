from PySide2 import QtCore
from PySide2.QtCore import Slot
from PySide2.QtWidgets import (
    QCheckBox,
    QComboBox,
    QHBoxLayout,
    QLabel,
    QLineEdit,
    QVBoxLayout,
    QWidget,
)
from tiffy import probe
from tiffy.probe import get_hdr_meta_data


class VideoStreamSetting(QWidget):
    def __init__(self, converter):
        QWidget.__init__(self)

        self.layout = QVBoxLayout()
        self.layout.setSpacing(0)
        self.layout.setMargin(0)

        self.layout.setContentsMargins(0, 0, 0, 0)
        self.converter = converter
        self.bitrate = QWidget()
        self.bitrate.setLayout(QHBoxLayout())
        self.copycheckbox = QCheckBox("copy")
        self.bitrate.layout().addWidget(self.copycheckbox)
        self.bitrate.layout().addWidget(self.copycheckbox)
        self.bitrate.layout().addWidget(QLabel("Bitrate (kB/s)"))
        self.bitrate.bitrate_edit = QLineEdit("1500")
        self.bitrate.bitrate_edit.setPlaceholderText("Enter value")
        self.bitrate.bitrate_edit.setInputMask("99999999999")
        self.bitrate.layout().addWidget(self.bitrate.bitrate_edit)

        mdata = get_hdr_meta_data(converter.path)

        self.hdrfix = None
        if mdata is not None:
            self.hdrfix = QWidget()
            self.hdrfix.layout = QHBoxLayout()
            self.hdrfix.chkbox = QCheckBox("Keep HDR")
            self.hdrfix.chkbox.setChecked(True)
            self.hdrfix.line = QLineEdit("")

            level = str(mdata["format_profile"])
            if level[0] == "L":
                level = level[1:]

            line = (
                '-x265-params "level='
                + str(level)
                + ":colorprim="
                + mdata["color_primaries"]
                + ":colormatrix="
                + mdata["color_space"]
                + ":transfer="
                + mdata["color_transfer"]
                + ":hdr=1:info=1:repeat-headers=1"
            )
            line = (
                line
                + ":max-cll="
                + str(mdata["mastering_display_luminance"][1])
                + ","
                + str(mdata["mastering_display_luminance"][0])
            )
            line = line + ":master-display=" + mdata["master-display"] + '"'
            self.hdrfix.line.setText(line)

            self.hdrfix.setLayout(self.hdrfix.layout)
            self.hdrfix.layout.addWidget(self.hdrfix.chkbox)
            self.hdrfix.layout.addWidget(self.hdrfix.line)

        self.codec = QComboBox()
        self.codec.addItem("h264")
        self.codec.addItem("libx265")
        self.codec.addItem("h264_nvenc")
        self.codec.addItem("hevc_nvenc")
        self.codec.addItem("h264_amf")
        self.codec.addItem("hevc_amf")

        self.layout.addWidget(self.codec)
        if self.hdrfix is not None:
            self.layout.addWidget(self.hdrfix)

        self.layout.addWidget(self.bitrate)

        self.setLayout(self.layout)
        self.copycheckbox.stateChanged.connect(self.copy)

        # connect everything to the update function
        self.copycheckbox.stateChanged.connect(self.update)
        self.bitrate.bitrate_edit.textChanged.connect(self.update)
        self.codec.currentIndexChanged.connect(self.update)

    @Slot()
    def copy(self, state):
        self.bitrate.bitrate_edit.setDisabled(state > 0)

    @Slot()
    def update(self):
        self.converter.update_commandline()


class VideoStreamWidget(QWidget):
    def __init__(self, converter, stream):
        QWidget.__init__(self)

        self.stream = stream
        self.layout = QHBoxLayout()
        self.checkbox = QCheckBox()
        self.layout.addWidget(self.checkbox)
        element = QLabel(probe.video_stream_representation(stream))
        self.layout.addWidget(element)
        self.setLayout(self.layout)
        self.converter = converter
        self.checkbox.stateChanged.connect(self.update)

        self.checkbox.setCheckState(QtCore.Qt.Checked)

    @Slot()
    def update(self):
        self.converter.update_commandline()


class VideoStreamsWidget(QWidget):
    def __init__(self, converter, streams):
        QWidget.__init__(self)

        self.layout = QVBoxLayout()
        self.streams = []
        for stream in streams:
            widget = VideoStreamWidget(converter, stream)
            self.streams.append(widget)
            self.layout.addWidget(widget)

        self.setLayout(self.layout)
