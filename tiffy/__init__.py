import os
import sys

import qdarkstyle
from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtGui import QDropEvent
from PySide2.QtWidgets import QApplication, QVBoxLayout, QWidget
from tiffy import probe
from tiffy.Converter import Converter

path = os.getcwd()
os.environ["PATH"] += os.pathsep + path


class Tiffy(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        self.setAcceptDrops(True)
        self.frames: list[Converter] = []

        pixmap = QtGui.QPixmap("assets/logo.png")
        label = QtWidgets.QLabel()
        pixmap = pixmap.scaled(500, 500, QtCore.Qt.KeepAspectRatio)
        label.setPixmap(pixmap)

        self.layout = QVBoxLayout()
        self.layout.addWidget(label)
        self.setLayout(self.layout)

    def dragEnterEvent(self, event):
        event.setAccepted(True)

    def dropEvent(self, event: QDropEvent):
        if not event.mimeData().hasUrls():
            return
        for url in event.mimeData().urls():
            filepath = url.toLocalFile()

            if not os.path.isfile(filepath):
                continue
            streams = probe.get_streams(filepath)

            widget = Converter(
                path=filepath,
                video_streams=probe.get_video_streams(streams),
                audio_streams=probe.get_audio_streams(streams),
                subtitle_streams=probe.get_subtitle_streams(streams),
            )

            widget.resize(800, 600)
            widget.show()
            self.frames.append(widget)


def main():
    app = QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet_pyside2())
    widget = Tiffy()
    widget.resize(500, 500)
    widget.setWindowTitle("Tiffy - a python gui for ffmpeg")
    widget.show()
    return app.exec_()
