import logging
from datetime import datetime, timedelta
from pathlib import Path
from typing import Optional

import ffmpeg
from pymediainfo import MediaInfo

Stream = dict
VideoStream = Stream
AudioStream = Stream
SubtitleStream = Stream


def get_stream_time_in_seconds(stream) -> int:
    if "tags" in stream and "DURATION" in stream["tags"]:
        timestamp = stream["tags"]["DURATION"]
        timestamp = timestamp[0 : len(timestamp) - 3]

        d = datetime.strptime(timestamp, "%H:%M:%S.%f").time()
        seconds = timedelta(
            hours=d.hour, minutes=d.minute, seconds=d.second
        ).total_seconds()
        return max(0, int(seconds))
    return 0


def get_streams(file: Path) -> list[Stream]:
    probe = ffmpeg.probe(file)
    return probe["streams"]


def get_video_streams(streams: list[Stream]) -> list[VideoStream]:
    video_streams = []
    for stream in streams:
        if stream["codec_type"] == "video":
            video_streams.append(stream)
    return video_streams


def get_audio_streams(streams: list[Stream]) -> list[VideoStream]:
    audio_streams = []
    for stream in streams:
        if stream["codec_type"] == "audio":
            audio_streams.append(stream)
    return audio_streams


def get_subtitle_streams(streams: list[Stream]) -> list[SubtitleStream]:
    subtitle_streams = []
    for stream in streams:
        if stream["codec_type"] == "subtitle":
            subtitle_streams.append(stream)
    return subtitle_streams


def get_stream_index(stream):
    return stream["index"]


def subtitle_stream_representation(stream) -> str:
    unknown = "unknown"
    if "tags" not in stream:
        return unknown
    tags = stream["tags"]
    if "title" in tags:
        lang = tags["title"]
    elif "language" in tags:
        lang = tags["language"]
    else:
        return unknown

    return f"{lang} ({stream['codec_name']})"


def audio_stream_representation(stream) -> str:
    representation: str = (
        str(stream["codec_name"])
        + " "
        + str(stream["channel_layout"])
        + " "
        + str(int(float(stream["sample_rate"]) / 1000.0))
        + " kHz"
    )

    if "tags" in stream:
        for key in stream["tags"]:
            representation = representation + " " + stream["tags"][key]

    return representation


def video_stream_representation(stream: VideoStream) -> str:
    bits = ""
    if "bits_per_raw_sample" in stream:
        bits = " " + str(stream["bits_per_raw_sample"]) + "bit"
    return (
        str(stream["codec_name"])
        + " "
        + str(stream["width"])
        + "x"
        + str(stream["height"])
        + bits
    )


def get_avg_fps(stream) -> float:
    frame_rate_format = str.split(stream["avg_frame_rate"], "/")
    fps = float(frame_rate_format[0])
    if len(frame_rate_format) > 1:
        fps = fps / float(frame_rate_format[1])
    return fps


def print_video_stream(stream: VideoStream):
    logging.warning(get_stream_index(stream))
    name = stream["codec_name"]
    width = stream["width"]
    height = stream["height"]
    aspect_ratio = stream["display_aspect_ratio"]
    pixel_format = stream["pix_fmt"]
    frame_rate_format = str.split(stream["avg_frame_rate"], "/")
    fps = float(frame_rate_format[0])
    if len(frame_rate_format) > 1:
        fps = fps / float(frame_rate_format[1])

    color_width = "SDR"

    if "bits_per_raw_sample" in stream and int(stream["bits_per_raw_sample"]) > 9:
        color_width = "HDR"

    logging.warning("Codec:", name)
    logging.warning("Resolution:", width, "x", height)
    logging.warning("Aspect Ratio:", aspect_ratio)
    logging.warning("Pixelformat:", pixel_format)
    logging.warning("AVG FPS:", fps)
    logging.warning("Colorwidth:", color_width)


def get_hdr_meta_data(path: Path) -> Optional[dict]:
    media_info = MediaInfo.parse(path)

    def isfloat(value):
        try:
            float(value)
            return True
        except ValueError:
            return False

    for track in media_info.tracks:
        if track.track_type == "Video":
            isNone = False
            items = {
                "commercial_name": track.commercial_name,
                "format_profile": str.split(track.format_profile, "@")[1],
                "bit_depth": track.bit_depth,
                "mastering_display_color_primaries": track.mastering_display_color_primaries,
                "mastering_display_luminance": track.mastering_display_luminance,
            }
            for key in items:
                if items[key] is None:
                    isNone = True
                    break

            if not isNone:
                minmax_luminance = items["mastering_display_luminance"] = [
                    int(float(s) / 0.00002)
                    for s in str(track.mastering_display_luminance).split()
                    if (isfloat(s))
                ]

                min_luminance = min(minmax_luminance[0], minmax_luminance[1])
                max_luminance = max(minmax_luminance[0], minmax_luminance[1])

                items["mastering_display_luminance"] = [min_luminance, max_luminance]

                primaries = items["mastering_display_color_primaries"]

                if "P3" in primaries:
                    primaries = (
                        "G(13250,34500)B(7500,3000)R(34000,16000)WP(15635,16450)"
                    )
                elif "709" in primaries:
                    primaries = (
                        "G(15000,30000)B(7500,3000)R(32000,16500)WP(15635,16450)"
                    )
                elif "2020" in primaries:
                    primaries = "G(8500,39850)B(6550,2300)R(35400,14600)WP(15635,16450)"
                else:
                    primaries = None

                if primaries is not None:
                    primaries += f"L({max_luminance},{min_luminance})"

                items["master-display"] = primaries

                streams = get_streams(file=path)
                video_streams = get_video_streams(streams=streams)

                if len(video_streams) > 0:
                    items["color_primaries"] = video_streams[0]["color_primaries"]
                    items["color_transfer"] = video_streams[0]["color_transfer"]
                    items["color_space"] = video_streams[0]["color_space"]

                logging.error(
                    "Error extracting HDR metadata, the file %s does not contain a video stream",
                    path,
                )
                return items
    return None
